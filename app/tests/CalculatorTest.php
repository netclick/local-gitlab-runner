<?php

namespace App\Tests;

use App\Service\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testSuccess(): void
    {
        $calculator = new Calculator();

        $this->assertSame(5, $calculator->add(2, 3));
    }

    public function testFailing(): void
    {
        $calculator = new Calculator();

        $this->assertNotSame(5, $calculator->add(3, 3));
    }
}
